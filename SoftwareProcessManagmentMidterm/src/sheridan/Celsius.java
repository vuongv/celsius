package sheridan;

public class Celsius {
	public static int fromFahrenheit (int degree) {
		return (int) Math.ceil((degree-32)*(5.0/9.0));
	}
}

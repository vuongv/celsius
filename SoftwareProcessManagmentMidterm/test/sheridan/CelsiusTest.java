package sheridan;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
/*
 * vuongv - Vinh Khoa Vuong
 */
class CelsiusTest {

	@Test
	public void testCelsiusRegular() {
		int convertedDegree = Celsius.fromFahrenheit(74);
		assertTrue("The degree provided does not match the result", convertedDegree == 24);
	}
	@Test
	public void testCelsiusException() {
		int convertedDegree = Celsius.fromFahrenheit(74);
		assertFalse("The degree provided does not match the result", convertedDegree == 23.1);
	}
	@Test
	public void testCelsiusBoundaryIn() {
		int convertedDegree = Celsius.fromFahrenheit(74);
		assertTrue("The degree provided does not match the result",convertedDegree == 24.0);
	}
	@Test
	public void testCelsiusBoundaryOut() {
		int convertedDegree = Celsius.fromFahrenheit(74);
		assertFalse("The degree provided does not match the result", convertedDegree == 24.1);
	}
	
}
